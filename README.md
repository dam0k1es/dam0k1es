<h1 align="center">Hi 👋, I'm Dam0k1es</h1>

<h3 align="center">Student working on his Master's thesis</h3>

<!--- - 🔭 I’m currently
-->
  

- 🌱 I’m currently learning about **RFID**, **Bluetooth** and **Wi-Fi** Hacking.

  

- 👨‍💻 All of my projects are available at [gitlab.com/dam0k1es](gitlab.com/dam0k1es)

  

- 💬 Ask me about **security**, **privacy** or **linux**


- 🕵️ Please help me with my [awesome-security-games](https://gitlab.com/dam0k1es/awesome-security-games) list
  

- 📫 How to reach me **dam0k1es@member.fsf.org**

  

<h3 align="left">Coding:</h3>

<p align="left"> <a href="https://www.linux.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/> </a> <a href="https://www.gnu.org/software/bash/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/gnu_bash/gnu_bash-icon.svg" alt="bash" width="40" height="40"/> </a> <a href="https://www.docker.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-original-wordmark.svg" alt="docker" width="40" height="40"/> </a> <a href="https://www.vagrantup.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/vagrantup/vagrantup-icon.svg" alt="vagrant" width="40" height="40"/> </a> <a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> <a href="https://www.python.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a> <a href="https://www.gtk.org/" target="_blank" rel="noreferrer"> <img src="https://upload.wikimedia.org/wikipedia/commons/7/71/GTK_logo.svg" alt="gtk" width="40" height="40"/> </a> <a href="https://pandas.pydata.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/2ae2a900d2f041da66e950e4d48052658d850630/icons/pandas/pandas-original.svg" alt="pandas" width="40" height="40"/> </a>  <a href="https://www.cprogramming.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg" alt="c" width="40" height="40"/> </a> <a href="https://www.arduino.cc/" target="_blank" rel="noreferrer"> <img src="https://cdn.worldvectorlogo.com/logos/arduino-1.svg" alt="arduino" width="40" height="40"/> </a> </a> <a href="https://flutter.dev" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/flutterio/flutterio-icon.svg" alt="flutter" width="40" height="40"/> </a> </p> 

<h3 align="left">Pentesting:</h3>

<img src="https://tryhackme-badges.s3.amazonaws.com/Dam0k1es.png" alt="TryHackMe" height="60" align="right">

<br>
<br>
<br>

<h3 align="left">Support:</h3>

 <img src="https://github.com/Dam0k1es/Dam0k1es/blob/main/img/fsf.png?raw=true" alt="Free Software Foundation" height="60" align="right"/>

‎ <br>
‎ <br>
‎ <br>
‎ <br>
‎ <br>
‎ 

